SELECT	
	ic.category_name,
	SUM(i.item_price) AS total_price
	
FROM
	item_category ic
INNER JOIN
	item i
ON
	i.category_id = ic.category_id
GROUP BY
	ic.category_name
ORDER BY
	total_price DESC
;